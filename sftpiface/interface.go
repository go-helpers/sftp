package sftpiface

import (
	"os"
	"time"

	"github.com/kr/fs"
	gosftp "github.com/pkg/sftp"
)

var _ ClientInterface = (*gosftp.Client)(nil)

// ClientInterface sftp.Client interface
type ClientInterface interface {
	Chmod(path string, mode os.FileMode) error
	Chown(path string, uid, gid int) error
	Chtimes(path string, atime time.Time, mtime time.Time) error
	Close() error
	Getwd() (string, error)
	Glob(pattern string) (matches []string, err error)
	Join(elem ...string) string
	Lstat(p string) (os.FileInfo, error)
	Mkdir(path string) error
	MkdirAll(path string) error
	Open(path string) (*gosftp.File, error)
	OpenFile(path string, f int) (*gosftp.File, error)
	PosixRename(oldname, newname string) error
	ReadDir(p string) ([]os.FileInfo, error)
	ReadLink(p string) (string, error)
	Remove(path string) error
	RemoveDirectory(path string) error
	Rename(oldname, newname string) error
	Stat(p string) (os.FileInfo, error)
	StatVFS(path string) (*gosftp.StatVFS, error)
	Symlink(oldname, newname string) error
	Truncate(path string, size int64) error
	Wait() error
	Walk(root string) *fs.Walker
}
