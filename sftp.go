package sftp

import (
	"strings"

	gosftp "github.com/pkg/sftp"
	"gitlab.com/go-helpers/sftp/sftpiface"
	"golang.org/x/crypto/ssh"
)

// NewClient returns a new sftpClient interface
func NewClient(host, sshPrivateKey, username string) (i sftpiface.ClientInterface, err error) {
	conn, err := newConnection(host, sshPrivateKey, username)
	if err != nil {
		return
	}
	i, err = gosftp.NewClient(conn)
	return
}

func newConnection(host, sshPrivateKey, username string) (conn *ssh.Client, err error) {
	sshPrivateKey = strings.Replace(sshPrivateKey, "\t", "", -1)
	signer, err := ssh.ParsePrivateKey([]byte(sshPrivateKey))
	if err != nil {
		return
	}
	return ssh.Dial("tcp", host, &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{ssh.PublicKeys(signer)},
	})
}
